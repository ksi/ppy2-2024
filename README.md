# PPY2 – Programming in Python 2

[![Static Badge](https://img.shields.io/badge/Synchronize%20to%20JupyterHub-08c?logo=jupyter&labelColor=white&color=08c)](
https://jupyter.fjfi.cvut.cz/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.fjfi.cvut.cz%2Fksi%2Fppy2-2024&urlpath=lab%2Ftree%2Fppy2-2024%2FREADME.md&branch=main)

## Useful links

- [Course information in study plans](https://bilakniha.cvut.cz/en/predmet7675306.html)
- [GitLab project](https://gitlab.fjfi.cvut.cz/ksi/ppy2-2024)
- [Information about CTU user account](https://it.fjfi.cvut.cz/en/navody/uzivatelsky-ucet/ucet-cvut)
- [FJFI JupyterHub](https://jupyter.fjfi.cvut.cz/)

## Requirements for completing the course

To successfully complete the course and earn the ungraded assessment ("zápočet"), you need to:

- attend at least 75% lectures/tutorials (9 of 12)
- be active in the seminar
- ask questions on the topic during discussion with the lecturer

## Schedule

Every Monday 16:00-17:40 in T-101.

1. September 23 – **Jakub Klinkovský (CTU-FNSPE)** – [Using Jupyter for Content Development](jupyter_content/presentation.ipynb)
2. September 30 – **Matej Mojzeš (CTU-FNSPE)** – [Jupyter Notebooks for Reproducible Research](reproducible_research/deck.ipynb)
3. October 7 – **Matej Mojzeš (CTU-FNSPE)** – [Object Oriented Programming and Heuristics](oop_heuristics/OOPS.ipynb)
4. October 14 – **Herman Goulet-Oullet (CTU-FIT)** – [SageMath](SageMath/intro.ipynb)
5. October 21 – **Petr Kubera (CTU-FNSPE)** – [Introduction to Machine Learning and Neural Networks](ML_intro/Python_ML_Introduction.ipynb)
6. November 4 – **Lukáš Bátrla (Cisco)** – [Machine Learning in Practice](practical_ML/FJFI_practical_ML_class.ipynb)
7. November 11 – **Adam Peichl (CTU-FME)** – [Practical Examples of Linear and Quadratic Programming](lpqp/01_intro.ipynb)
8. November 18 – **Adam Peichl (CTU-FME)** – [Introduction to Control Theory and Signal Processing](control/01_control_intro.ipynb)
9. November 25 – **Matej Mojzeš (CTU-FNSPE)** – [Python for Data Processing in Google Cloud](gcp_data_processing/deck.ipynb)
10. December 2 – **Petr Fialka (Czech Television)** – [Python Development in Czech Television](https://github.com/petrfialka/Python-BE-API-Lesson)
11. December 9 – **Jakub Klinkovský (CTU-FNSPE)** – [Python and C++ interoperability](PythonCXX/PythonCXX.ipynb)
12. December 16 – **Vladimír Jarý (CTU-FNSPE)** – Development of desktop user interfaces with PyQt
