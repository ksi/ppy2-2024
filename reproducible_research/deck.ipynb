{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2de09923",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Jupyter notebooks for reproducible research\n",
    "\n",
    "...including software engineering best practices, such as \"code review\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "299e0a29",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "## You again?\n",
    "\n",
    "* Yes, I am glad you remembered 🙂\n",
    "* Just for the record, my name is Matej Mojzeš\n",
    "* TL;DR — I got a PhD on FNSPE in the field of Integer Optimization Heuristics (more on that later) and my primary occupation is in a company called [SharpGrid](https://www.sharpgrid.com), as the _Head of Data_\n",
    "  - one of my goals is to **look after best practices in research, data analysis, and modelling**, in line with concepts that we will talk about in the upcoming minutes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d67efb38",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Just a quick headsup / disclaimer\n",
    "- this will be a tutorial, designed to fill the time and space we have available today\n",
    "- there will be some less related details (modules, ipython magics, etc) in this work, that might look like a distraction, but should we worth looking at on their own (eg. [statsmodels](https://www.statsmodels.org/stable/index.html), or [UCI repo](https://archive.ics.uci.edu/datasets))\n",
    "- feel free to stop me and ask questions at any point\n",
    "- you will get the notebooks from this talk, but they do not contain 100% of the information — rather make your own notes, as you see fit\n",
    "- **I might be wrong, argue with me** 🙏"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "798cb2ea",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## OK, let's imagine you are in the following situation...\n",
    "\n",
    "* You have to develop a **model for predicting the internal combusion engine (ICE) power output** (given the parameters of an engine, we want the horsepower it can generate)\n",
    "* You want to transparently develop your work, document it, ensure there will be no mistakes, and publish it for anyone to use"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ea0c526",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* Sounds like research, right?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e30854df",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* How hard can it be?\n",
    "\n",
    "<img src=\"https://miro.medium.com/v2/resize:fit:1200/0*aQHps44iwUqEkbzI.jpg\">"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a4848eb",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "...actually, not that hard, if you have the right data. But this (ie. data) is the tricky part, which will typically eat most of your time and effort. So how to make it a bit easier?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41328844",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Firstly, let's use the Jupyter notebook to: \n",
    "* explore the data, \n",
    "* develop code, run it, evaluate results,\n",
    "* present the results\n",
    "* iterate\n",
    "\n",
    "All in one place, transparently, in a reproducible and reviewable manner. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe4240a3",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "BTW a couple more reasons to consider notebooks: http://www.nature.com/news/interactive-notebooks-sharing-the-code-1.16261"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8495ec11",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "For now, let's dig into the data in `ice_power_prediction.ipynb`..."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7921bc92",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## What will happen next?\n",
    "\n",
    "\n",
    "1. After (first version of) the notebook is made, it will be pushed into a git feature branch\n",
    "   - together with the plain-python version, purely for review purposes\n",
    "2. We will create a pull request\n",
    "3. Review the notebook, post a comment / proposal for an update\n",
    "4. Push a higher version\n",
    "5. Review again and merge"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45aee697",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## How to improve the process further\n",
    "\n",
    "1. Ensure even better reproducibility — use [virtual environments](https://docs.python.org/3/library/venv.html), or [docker](https://kordinglab.com/2022/10/28/LabTeaching-Docker-for-Science.html) containers (see appendix as well!)\n",
    "2. Make notebooks available to non-technical users, eg. via [nbviewer](https://nbviewer.org)\n",
    "3. Work with big(-ish) data\n",
    "   - hardly possible to version-control larger data sets\n",
    "   - rather enforce [data immutability](https://www.oreilly.com/library/view/operationalizing-the-data/9781492049517/ch04.html) in your organization, and reference path to cloud storage\n",
    "4. For more complex tasks, create a repository/folder structure with:\n",
    "    1. development notebooks — can be add-only, single owner for each one, dead ends are expected\n",
    "    2. deliverable notebooks — can and should be updated to contain the latest and greatest code, results and conclusions, made collaboratively and meant to be published\n",
    "    3. any artifacts, that need to be referenced independently (modules, figures, models...)\n",
    "5. For more complex tasks, a task/issue tracking system like [JIRA](https://www.atlassian.com/software/jira) can be integrated (at least by reusing ticket IDs in filenames)\n",
    "\n",
    "More reading materials here: https://www.reviewnb.com/git-jupyter-notebook-ultimate-guide (highly recommended!)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d16fe6fb",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Thank you for your attention\n",
    "\n",
    "Feel free to reach out with any questions!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "decabf73",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Appendix I\n",
    "\n",
    "To automatically save a plain `.py` file together with the `.ipynb` notebook:\n",
    "\n",
    "Place the following code to `~/.jupyter/jupyter_notebook_config.py`. If you do not have such file, you can run `jupyter notebook --generate-config` to create the default one.\n",
    "\n",
    "```\n",
    "import os\n",
    "from subprocess import check_call\n",
    "\n",
    "def post_save(model, os_path, contents_manager):\n",
    "    \"\"\"\n",
    "    post-save hook for converting notebooks to .py files\n",
    "    source: https://gist.github.com/jbwhit/881bdeeaae3e4128947c\n",
    "    \"\"\"\n",
    "    if model['type'] != 'notebook':\n",
    "        return # only do this for notebooks\n",
    "    d, fname = os.path.split(os_path)\n",
    "    check_call(['jupyter', 'nbconvert', '--to', 'script', fname], cwd=d)\n",
    "\n",
    "c.FileContentsManager.post_save_hook = post_save\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b8b57496",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "BTW, author of the snippet (Jonathan Whitmore) is definitely worth following. (Also other parts of this talk are inspired by his work)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55a178af",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Appendix II\n",
    "\n",
    "Have a look at the [Turing Way](https://book.the-turing-way.org) initiative on reproducible research. Especially, there is a nice tutorial related to Docker containers mentioned above: https://book.the-turing-way.org/reproducible-research/renv/renv-containers."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
