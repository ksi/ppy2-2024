#define PY_SSIZE_T_CLEAN    // Make "s#" use `Py_ssize_t` rather than `int`
#include <Python.h>

// This is the function that we want to bind/wrap to Python
long factorial(long n) {
    long result = 1;
    while (n > 0) {
        result *= n--;
    }
    return result;
}

// This is the definition of a method
PyObject* wrap_factorial(PyObject* self, PyObject* args) {
    // Parsing of the input arguments
    long n;
    if (!PyArg_ParseTuple(args, "l", &n)) {
        return NULL;
    }
    // Error handling (Python will raise an exception)
    if (n < 0) {
        PyErr_Format(PyExc_ValueError, "Factorial of a negative value %ld is not defined.", n);
        return NULL;
    }
    // The main computation in C
    long result = factorial(n);
    // Conversion of the result to a Python object
    return PyLong_FromLong(result);
}

// Exported methods are collected in a table
PyMethodDef method_table[] = {
    {"factorial", (PyCFunction) wrap_factorial, METH_VARARGS, "A function that computes the factorial of an integer"},
    {NULL, NULL, 0, NULL} // Sentinel value ending the table
};

// A struct that contains the definition of a module
PyModuleDef demo_module = {
    PyModuleDef_HEAD_INIT,
    "demo", // Module name
    "This is the module docstring",
    -1,     // Optional size of the module state memory
    method_table
};

// The module init function
PyMODINIT_FUNC PyInit_demo(void) {
    return PyModule_Create(&demo_module);
}