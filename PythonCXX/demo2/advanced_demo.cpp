#include "mysource.h"
#include <iostream>
#include <pybind11/pybind11.h>

namespace py = pybind11;

void call_python(py::function callback) {
    std::cout << "message from C++" << std::endl;

    // create a C++ object
    Pet cat("Kitty");

    // call a Python function
    py::object result = callback(std::ref(cat));

    // verify the result and cat name
    std::cout << "result in C++ is " << result.cast<int>() << " and cat name is " << cat.getName() << std::endl;
}

// Definition of the Python module
PYBIND11_MODULE(advanced_demo, m) {
    // set optional docstring of the module
    m.doc() = "This is an advanced demo that demonstrates how to call a Python function from C++";

    // define members of the module
    m.def("call_python", &call_python, py::arg("callback"), "Simple function which calls from C++ into Python.");
}
