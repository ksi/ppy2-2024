#pragma once

#include <string>

// This is a declaration of the function that we want to bind to Python
long factorial(long n);

// This is a declaration of a global variable
extern long global_variable;

// This is a definition of some class that we want to bind to Python
class Pet {
public:
    Pet(const std::string& name);
    void setName(const std::string& name);
    const std::string& getName() const;

private:
    std::string name;
};