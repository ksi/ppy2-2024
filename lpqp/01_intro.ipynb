{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimisation - linear and quadratic programs\n",
    "\n",
    "Adam Peichl (adam.peichl@fs.cvut.cz), Mechanical Engineering - Department of Instrumentation and Control, Czech Technical University\n",
    "\n",
    "What are goals of this tutorial:\n",
    "1. introduction to `cvxpy` - [official cvxpy documentation](https://www.cvxpy.org/)\n",
    "1. try to convert real problem into code\n",
    "1. having fun solving easy linear and quadratic problems\n",
    "\n",
    "What definitely are not goals:\n",
    "1. go deep into mathematics\n",
    "1. go through advanced features of `cvxpy`\n",
    "\n",
    "## Schedule of this lecture\n",
    "1. introduction (What is optimisation, what is linear programing, ...)\n",
    "1. cvxpy library (basic classes, interface, solvers)\n",
    "1. solving multiple simple problems together"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dependencies\n",
    "1. I will use `python 3.10`\n",
    "1. packages, `cvxpy` , `numpy`, `matplotlib` + standard libraries\n",
    "1. I assume basic knowledge of `numpy` and `matplotlib`\n",
    "\n",
    "You should be able to install these libraries via\n",
    "```\n",
    "!pip install cvxpy, numpy, matplotlib\n",
    "```\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction to optimization\n",
    "\n",
    "Optimization is a process of seeking the maximum or minimum of the *objective function*. In reality, those types of problems are all over the place and if you think about it you can come to a conclusion that everything is an optimization.\n",
    "\n",
    "For us, optimization problem will be formalized as follows:\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "    \\mathrm{minimize} & \\quad f_{0}(x) \\\\\n",
    "    \\mathrm{subject \\ to} & \\quad f_{i}(x) \\leq b_i \\quad i=1,\\dots,n\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "where vector $x$ is called *vector of decision variables*, $f_{0}(x)$ is called *objective* function and $f_{i}(x) \\leq b_i$ are called *constraints*.\n",
    "\n",
    "Optimal solution (or solutions) can be defined as:\n",
    "$$\n",
    "\\begin{equation}\n",
    "    x^{*} = \\mathrm{inf}\\{f_{0}(x) | x \\in \\mathcal{A}\\}\n",
    "\\end{equation}\n",
    "$$\n",
    "\n",
    "where $\\mathcal{A}$ is *feasible set* defined via constraints $f_{i}(x) \\leq b_i$. If you do not know what $\\mathrm{inf}$ (infimum) is just think of it as *minimum*.\n",
    "\n",
    "Some notable examples of optimization problems:\n",
    "1. LS (Least squares problem)\n",
    "1. LP (Linear Programming)\n",
    "1. QP (Quadratic Programming)\n",
    "1. MILP (Mixed Integer Linear Programming)\n",
    "1. Convex Programming (LS, LP and also QP are in this set of problems)\n",
    "1. ... and much more\n",
    "\n",
    "In theory most optimization problems we have no idea how to solve them. In reality though we can approximate or transform a lot of problems to a form we can solve."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Least Squares example\n",
    "\n",
    "formulation:\n",
    "$$\n",
    "\\begin{align}\n",
    "    \\mathrm{minimize} & \\quad ||Ax-b||^2_2 \\\\\n",
    "    \\mathrm{subject \\ to} & \\quad -\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "We know exact solution:\n",
    "$$\n",
    "\\begin{equation}\n",
    "    x^* = (A^T A)^{-1}A^T b\n",
    "\\end{equation}\n",
    "$$\n",
    "\n",
    "### Linear Program\n",
    "\n",
    "formulation:\n",
    "$$\n",
    "\\begin{align}\n",
    "    \\mathrm{minimize} & \\quad c^Tx \\\\\n",
    "    \\mathrm{subject \\ to} & \\quad Ax \\leq b\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "### Quadratic Program\n",
    "\n",
    "formulation:\n",
    "$$\n",
    "\\begin{align}\n",
    "    \\mathrm{minimize} & \\quad \\frac{1}{2}x^THx + c^Tx \\\\\n",
    "    \\mathrm{subject \\ to} & \\quad Ax \\leq b\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Introduction to `cvxpy`\n",
    "\n",
    "> CVXPY is an open source Python-embedded modeling language for convex optimization problems. It lets you express your problem in a natural way that follows the math, rather than in the restrictive standard form required by solvers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "import cvxpy as cp\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['CLARABEL', 'ECOS', 'ECOS_BB', 'OSQP', 'SCIPY', 'SCS']"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cp.installed_solvers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `Variable`, `Constraint` and `Expression`\n",
    "\n",
    "- basic building blocks for formulating our problem\n",
    "- [Api Reference](https://www.cvxpy.org/api_reference/cvxpy.html)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Variable((), var19, nonneg=True)"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "x = cp.Variable(nonneg=True)\n",
    "x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Expression(AFFINE, UNKNOWN, ())"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "expr1 = x - 1\n",
    "expr1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Inequality(Constant(CONSTANT, NONNEGATIVE, ()))"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "constr1 = x >= 20\n",
    "constr1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### atoms\n",
    "\n",
    "- functions which takes `Expression` and returns `Expression`\n",
    "- [Api Reference - Atoms](https://www.cvxpy.org/api_reference/cvxpy.atoms.html)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "var19 + 1.0 is concave: True, convex: True\n"
     ]
    }
   ],
   "source": [
    "expr = x + 1\n",
    "print(f\"{expr} is concave: {expr.is_atom_concave()}, convex: {expr.is_atom_convex()}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Inequality(Variable((10,), varvec))"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "variable_vector = cp.Variable(10, name=\"varvec\")\n",
    "lower_bound =  np.full(10, fill_value=10.0)\n",
    "\n",
    "lower_bound >= variable_vector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Inequality(Variable((10,), varvec))"
      ]
     },
     "execution_count": 31,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "variable_vector <= lower_bound # representing same thing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `Problem` and objectives `Minimize` and `Maximize`\n",
    "\n",
    "- Problem consists of *objective* - function which determines, which solution is better then other and list of *constraints*, which limits set of possible solutions\n",
    "- [Api Reference - Problem, objective](https://www.cvxpy.org/api_reference/cvxpy.problems.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {},
   "outputs": [],
   "source": [
    "x1 = cp.Variable(nonneg=True)\n",
    "x2 = cp.Variable(nonneg=True)\n",
    "\n",
    "limitation = x1 + x2 <= 10.0\n",
    "f0 = 1.5*x1 + x2\n",
    "objective = cp.Maximize(f0)\n",
    "problem = cp.Problem(objective, [limitation])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 61,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 61,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "problem.is_qp()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `Solver`\n",
    "- muscles that solve problem we formulated\n",
    "- `cvxpy` takes care of correct formulation for solver\n",
    "- [list of solvers](https://www.cvxpy.org/tutorial/advanced/index.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "14.999999998797982"
      ]
     },
     "execution_count": 62,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "problem.solve()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "SolverStats(solver_name='ECOS', solve_time=3.24e-05, setup_time=3.99e-05, num_iters=6, extra_stats={'x': array([1.00000000e+01, 6.48395792e-10]), 'y': array([], dtype=float64), 'z': array([2.37324676e-10, 5.00000000e-01, 1.50000000e+00]), 's': array([1.00000000e+01, 6.51008666e-10, 5.87827625e-10]), 'info': {'exitFlag': 0, 'pcost': -14.999999998797982, 'dcost': -15.000000002373001, 'pres': 1.5072453126204284e-13, 'dres': 9.033015198299394e-15, 'pinf': 0.0, 'dinf': 0.0, 'pinfres': nan, 'dinfres': 0.5000000000617459, 'gap': 3.592632437845269e-09, 'relgap': 2.3950882920887753e-10, 'r0': 1e-08, 'iter': 6, 'mi_iter': -1, 'infostring': 'Optimal solution found', 'timing': {'runtime': 7.23e-05, 'tsetup': 3.99e-05, 'tsolve': 3.24e-05}, 'numerr': 0}})"
      ]
     },
     "execution_count": 63,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "problem.solver_stats"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'optimal'"
      ]
     },
     "execution_count": 64,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "problem.status"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `Variable` and `Expression` were backpopulated"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 67,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Optimal x = [9.999999998766391, 6.483957919308268e-10]\n",
      "optimal objective: 14.999999998797982\n"
     ]
    }
   ],
   "source": [
    "print((f\"Optimal x = [{x1.value}, {x2.value}]\\n\"\n",
    "      f\"optimal objective: {f0.value}\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Additional Materials\n",
    "\n",
    "If you are interested in additional materials, there is very informative youtube video from MIT courseware:\n",
    "\n",
    "[![15. Linear Programming: LP, reductions, Simplex](http://img.youtube.com/vi/WwMz2fJwUCg/0.jpg)](https://www.youtube.com/watch?v=WwMz2fJwUCg&t \"15. Linear Programming: LP, reductions, Simplex\")\n",
    "\n",
    "I also recommend Stanford Convex Optimisation lectures from prof. Boyd:\n",
    "\n",
    "- [Lecture Collection - Convex Optimization](https://www.youtube.com/playlist?list=PL3940DD956CDF0622)\n",
    "- [Book](https://web.stanford.edu/~boyd/cvxbook/bv_cvxbook.pdf)\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
